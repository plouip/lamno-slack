import React from 'react';
import { Route, Switch } from 'react-router';

import WorkspaceSelection from '../containers/WorkspaceSelection';
import Login from '../containers/Login';
import Workspace from '../containers/Workspace';

const Routes: React.FC = () => (
  <Switch>
    <Route exact path="/" component={WorkspaceSelection} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/workspace" component={Workspace} />
  </Switch>
);

export default Routes;
