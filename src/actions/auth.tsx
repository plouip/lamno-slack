import { ThunkResult } from '../utils/types';
import axios from 'axios';
import qs from 'qs';
import { Dispatch } from 'redux';
import { RootState } from '../reducers';

export const FIND_WORKSPACE = '@auth/find-workspace';
export const FIND_WORKSPACE_SUCCESS = '@auth/find-workspace-success';
export const FIND_WORKSPACE_FAILED = '@auth/find-workspace-failed';

export const SIGNIN_WORKSPACE = '@auth/signin-workspace';
export const SIGNIN_WORKSPACE_SUCCESS = '@auth/signin-workspace-success';
export const SIGNIN_WORKSPACE_FAILED = '@auth/signin-workspace-failed';

const baseUrl = process.env.REACT_APP_SLACK_API;

export const findWorkspace = (
  workspace: string
): ThunkResult<Promise<void>> => async (dispatch: Dispatch) => {
  dispatch({ type: FIND_WORKSPACE });

  const result = await axios.post(
    `${baseUrl}/auth.findTeam`,
    qs.stringify({ domain: workspace }),
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
  );

  if (result.data.ok) {
    // Remove "https://" and "/" from domain.
    const url = result.data.url.replace(/https:\/\/|\//g, '');
    const data = {
      name: workspace,
      teamId: result.data.team_id,
      url: url,
      emailDomains: result.data.email_domains
    };

    dispatch({ type: FIND_WORKSPACE_SUCCESS, payload: data });
    return Promise.resolve();
  } else {
    dispatch({
      type: FIND_WORKSPACE_FAILED,
      payload: result.data.error
    });
    return Promise.reject();
  }
};

export const signInWorkspace = (
  email: string,
  password: string,
  remember: boolean
): ThunkResult<Promise<void>> => async (
  dispatch: Dispatch,
  getState: () => RootState
) => {
  dispatch({ type: SIGNIN_WORKSPACE });

  const result = await axios.post(
    `${baseUrl}/auth.signin`,
    qs.stringify({
      email: email,
      password: password,
      team: getState().auth.domain.teamId
    }),
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
  );

  if (result.data.ok) {
    const data = {
      id: result.data.user,
      token: result.data.token,
      email: result.data.email
    };

    dispatch({
      type: SIGNIN_WORKSPACE_SUCCESS,
      payload: data
    });
    return Promise.resolve();
  } else {
    dispatch({
      type: SIGNIN_WORKSPACE_FAILED,
      payload: result.data.error
    });
    return Promise.reject();
  }
};
