import React from 'react';
import './workspace-selection-form.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import Card from '../common/Card';
import Input from '../common/Input';
import ButtonWithSpinner from '../common/ButtonWithSpinner';
import Alert from '../common/Alert';

interface Props {
  error?: string;
  onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
  onWorkspaceNameChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  submitting: boolean;
}

const WorkspaceSignInForm: React.FC<Props> = ({
  error,
  onSubmit,
  onWorkspaceNameChange,
  submitting = false
}) => {
  return (
    <div className="ws-selection-form">
      {error === 'team_not_found' && (
        <Alert type="error" className="ws-selection-form-alert">
          <strong>We couldn’t find your workspace.</strong>&nbsp;If you can’t
          remember your workspace’s address, we can&nbsp;
          <a href="#">send you a reminder</a>.
        </Alert>
      )}
      <Card className="ws-selection-form-card">
        <h1>Sign in to your workspace</h1>
        <form onSubmit={onSubmit} noValidate={true}>
          <p>
            Enter your workspace's <strong>Slack URL</strong>.
          </p>
          <p className="ws-selection-form-input">
            <Input
              id="workspace"
              name="workspace"
              placeholder="your-workspace-url"
              onChange={onWorkspaceNameChange}
            />
            <span className="domain">.slack.com</span>
          </p>
          <p className="ws-selection-form-submit">
            <ButtonWithSpinner
              type="submit"
              disabled={submitting}
              showSpinner={submitting}
              size="large"
              block
            >
              Continue
              <i>
                <FontAwesomeIcon icon={faArrowRight} />
              </i>
            </ButtonWithSpinner>
          </p>
          <p>
            Don't know your workspace URL?&nbsp;
            <a href="#">Find your workspace</a>
          </p>
        </form>
      </Card>
    </div>
  );
};

export default WorkspaceSignInForm;
