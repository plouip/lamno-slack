import React from 'react';
import './page-base.scss';

const Base: React.FC = ({ children }) => {
  return (
    <div className="page-base">
      <div className="page-content">{children}</div>
    </div>
  );
};

export default Base;
