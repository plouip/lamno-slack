import React from 'react';
import './app-header.scss';
import { Link } from 'react-router-dom';
import { NavContainer, NavBrand, NavItems, NavItem } from '../common/Navbar';
import Button from '../common/Button';

const AppHeader = () => {
  return (
    <NavContainer>
      <NavBrand>
        <a className="app-header-logo" href="https://slack.com" />
      </NavBrand>
      <NavItems>
        <NavItem>
          <Link className="app-header-link" to="#">
            Product
          </Link>
        </NavItem>
        <NavItem>
          <Link className="app-header-link" to="#">
            Pricing
          </Link>
        </NavItem>
        <NavItem>
          <Link className="app-header-link" to="#">
            Support
          </Link>
        </NavItem>
        <NavItem>
          <Link className="app-header-link" to="#">
            Create a new workspace
          </Link>
        </NavItem>
        <NavItem>
          <Link className="app-header-link" to="#">
            Find your workspace
          </Link>
        </NavItem>
        <NavItem className={'btn-sign-in'}>
          <Button variant="outlined" onClick={() => {}}>
            Sign in
          </Button>
        </NavItem>
      </NavItems>
    </NavContainer>
  );
};

export default AppHeader;
