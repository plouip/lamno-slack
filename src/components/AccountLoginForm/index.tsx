import React from 'react';
import './account-login-form.scss';
import Alert from '../common/Alert';
import ButtonWithSpinner from '../common/ButtonWithSpinner';
import Card from '../common/Card';
import Checkbox from '../common/Checkbox';
import Input from '../common/Input';

interface Props {
  domain: string;
  error?: string;
  onEmailChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onPasswordChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onRememberMeChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
  submitting: boolean;
  workspace: string;
}

const AccountLoginForm: React.FC<Props> = ({
  domain,
  error,
  onEmailChange,
  onPasswordChange,
  onRememberMeChange,
  onSubmit,
  submitting,
  workspace
}) => {
  return (
    <div className="account-login-form">
      {error === 'incorrect_password' && (
        <Alert type="error" className="account-login-form-alert">
          Sorry, you entered an incorrect email address or password.
        </Alert>
      )}
      {error === 'ratelimited' && (
        <Alert type="error" className="account-login-form-alert">
          <strong>Too many login failures!</strong>
          <div>
            Apologies! Too many incorrect codes were entered in too short a
            time. Please wait a few moments before trying again.
          </div>
        </Alert>
      )}
      <Card className="account-login-form-card">
        <h1 className="account-login-form-header">
          Sign in to&nbsp;
          <span className="account-login-form-workspace">{workspace}</span>
        </h1>
        <div className="account-login-form-domain">{domain}</div>
        <form onSubmit={onSubmit} noValidate={true}>
          <p>
            Enter your <strong>email address</strong> and{' '}
            <strong>password</strong>.
          </p>
          <Input
            name="email"
            placeholder="you@example.com"
            onChange={onEmailChange}
          />
          <Input
            type="password"
            name="password"
            placeholder="password"
            onChange={onPasswordChange}
          />
          <p className="account-login-form-submit">
            <ButtonWithSpinner
              type="submit"
              size="large"
              block
              disabled={submitting}
              showSpinner={submitting}
            >
              Sign in
            </ButtonWithSpinner>
          </p>
          <p>
            <Checkbox
              label="Remember me"
              name="rememberMe"
              onChange={onRememberMeChange}
            />
          </p>
          <p className="account-login-forgot-credentials">
            <a href="#">Forgot Password?</a>&nbsp;·&nbsp;
            <a href="#">Forgot which email you used?</a>
          </p>
        </form>
      </Card>
    </div>
  );
};

export default AccountLoginForm;
