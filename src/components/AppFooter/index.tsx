import React from 'react';
import './app-footer.scss';
import { FooterContainer, FootNote } from '../common/Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSlack,
  faTwitter,
  faYoutube
} from '@fortawesome/free-brands-svg-icons';

const AppFooter = () => {
  return (
    <FooterContainer className="app-footer">
      <section>
        <div className="app-footer-links">
          <div>
            <ul>
              <li className="app-footer-group-title title-1">Using Slack</li>
              <li>
                <a href="#">Product</a>
              </li>
              <li>
                <a href="#">Enterprise</a>
              </li>
              <li>
                <a href="#">Pricing</a>
              </li>
              <li>
                <a href="#">Support</a>
              </li>
              <li>
                <a href="#">Slack Guides</a>
              </li>
              <li>
                <a href="#">App Directory</a>
              </li>
              <li>
                <a href="#">API</a>
              </li>
            </ul>
          </div>
          <div>
            <ul>
              <li className="app-footer-group-title title-2">Slack ♥</li>
              <li>
                <a href="#">Jobs</a>
              </li>
              <li>
                <a href="#">Customers</a>
              </li>
              <li>
                <a href="#">Developers</a>
              </li>
              <li>
                <a href="#">Events</a>
              </li>
              <li>
                <a href="#">Blog</a>
              </li>
            </ul>
          </div>
          <div>
            <ul>
              <li className="app-footer-group-title title-3">Legal</li>
              <li>
                <a href="#">Privacy</a>
              </li>
              <li>
                <a href="#">Security</a>
              </li>
              <li>
                <a href="#">Terms of Service</a>
              </li>
              <li>
                <a href="#">Policies</a>
              </li>
            </ul>
          </div>
          <div>
            <ul>
              <li className="app-footer-group-title title-4">Handy Links</li>
              <li>
                <a href="#">Download desktop app</a>
              </li>
              <li>
                <a href="#">Download mobile app</a>
              </li>
              <li>
                <a href="#">Brand Guidelines</a>
              </li>
              <li>
                <a href="#">Slack at Work</a>
              </li>
              <li>
                <a href="#">Status</a>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <FootNote className="app-footer-footnote">
        <section>
          <i className="app-footnote-slack">
            <FontAwesomeIcon icon={faSlack} />
          </i>
          <ul className="app-footnote-contact">
            <li>
              <a href="#">Contact Us</a>
            </li>
            <li>
              <i className="app-footnote-twitter">
                <FontAwesomeIcon icon={faTwitter} />
              </i>
            </li>
            <li>
              <i className="app-footnote-youtube">
                <FontAwesomeIcon icon={faYoutube} />
              </i>
            </li>
          </ul>
        </section>
      </FootNote>
    </FooterContainer>
  );
};

export default AppFooter;
