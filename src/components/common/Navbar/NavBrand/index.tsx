import React from 'react';
import './nav-brand.scss';

const NavBrand: React.FC = ({ children }) => {
  return <span className="nav-brand">{children}</span>;
};

export default NavBrand;
