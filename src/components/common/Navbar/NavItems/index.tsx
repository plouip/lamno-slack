import React from 'react';
import './nav-items.scss';

const NavItem: React.FC = ({ children }) => {
  return <ul className="nav-items">{children}</ul>;
};

export default NavItem;
