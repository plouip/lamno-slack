import React from 'react';
import './nav-item.scss';
import clsx from 'clsx';

interface Props {
  className?: string;
}

const NavItem: React.FC<Props> = ({ children, className }) => {
  return <li className={clsx('nav-item', className)}>{children}</li>;
};

export default NavItem;
