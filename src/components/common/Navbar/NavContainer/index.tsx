import React from 'react';
import './nav-container.scss';

const Navbar: React.FC = ({ children }) => {
  return <nav className="top">{children}</nav>;
};

export default Navbar;
