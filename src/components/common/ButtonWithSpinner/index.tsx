import React from 'react';
import './button-with-spinner.scss';
import clsx from 'clsx';
import Button, { Props as ButtonProps } from '../Button';
import Spinner from '../Spinner';

interface Props extends ButtonProps {
  showSpinner?: boolean;
}

const ButtonWithSpinner: React.FC<Props> = ({
  children,
  showSpinner,
  ...rest
}) => {
  return (
    <Button {...rest}>
      <span className={clsx('btn-spinner-label', { active: showSpinner })}>
        {children}
      </span>
      {showSpinner && (
        <span className={clsx('btn-spinner-icon')}>
          <div>
            <span>
              <Spinner />
            </span>
          </div>
        </span>
      )}
    </Button>
  );
};

export default ButtonWithSpinner;
