import React from 'react';
import './card.scss';
import clsx from 'clsx';

interface Props {
  className?: string;
}

const Card: React.FC<Props> = ({ className, children }) => {
  return <div className={clsx('card', className)}>{children}</div>;
};

export default Card;
