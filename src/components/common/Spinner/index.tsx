import React from 'react';
import './spinner.scss';

interface Props extends React.HTMLAttributes<HTMLDivElement> {}

const Spinner: React.FC<Props> = props => {
  return (
    <div className="spinner">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Spinner;
