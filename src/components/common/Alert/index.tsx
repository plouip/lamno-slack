import React from 'react';
import './alert.scss';
import clsx from 'clsx';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

interface Props extends React.HTMLAttributes<HTMLDivElement> {
  type: 'success' | 'error' | 'warning';
}

const Alert: React.FC<Props> = ({ className, children, type, ...rest }) => {
  return (
    <div className={clsx('alert', `alert-${type}`, className)} {...rest}>
      <i>
        {type === 'error' && (
          <FontAwesomeIcon color="#e01e5a" icon={faExclamationTriangle} />
        )}
      </i>
      {children}
    </div>
  );
};

export default Alert;
