import React from 'react';
import './button.scss';
import clsx from 'clsx';

export interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  block?: boolean;
  onClick?: () => void;
  size?: 'small' | 'large';
  variant?: 'primary' | 'outlined';
}

const Button: React.FC<Props> = ({
  block,
  className,
  children,
  size,
  variant = 'primary',
  ...rest
}) => {
  return (
    <button
      className={clsx(
        'btn',
        `btn-${variant}`,
        {
          'full-width': block,
          [`btn-${size}`]: size != null
        },
        className
      )}
      {...rest}
    >
      {children}
    </button>
  );
};

export default Button;
