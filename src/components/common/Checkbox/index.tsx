import React from 'react';
import './checkbox.scss';

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  label: string;
}

const Checkbox: React.FC<Props> = ({ label, ...rest }) => {
  return (
    <label className="checkbox-label">
      <input type="checkbox" {...rest} className="checkbox" />
      {label}
    </label>
  );
};

export default Checkbox;
