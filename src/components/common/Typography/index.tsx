import React from 'react';
import './typography.scss';
import clsx from 'clsx';

interface Props extends React.HTMLAttributes<HTMLParagraphElement> {
  align?: 'left' | 'center' | 'right';
}

const Typography: React.FC<Props> = ({ align, children, ...rest }) => {
  return (
    <p className={clsx({ [`typography-${align}`]: align != null })} {...rest}>
      {children}
    </p>
  );
};

export default Typography;
