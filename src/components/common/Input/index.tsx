import React from 'react';
import './input.scss';
import clsx from 'clsx';

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  className?: string;
  rest?: string[];
}

const Input: React.FC<Props> = ({ className, ...rest }) => {
  return <input className={clsx('text-input', className)} {...rest} />;
};

export default Input;
