import React from 'react';
import './footer-container.scss';
import clsx from 'clsx';

interface Props extends React.HTMLAttributes<HTMLElement> {}

const FooterContainer: React.FC<Props> = ({ className, children, ...rest }) => {
  return (
    <footer className={clsx('footer', className)} {...rest}>
      {children}
    </footer>
  );
};

export default FooterContainer;
