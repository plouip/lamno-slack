import React from 'react';
import './footnote.scss';
import clsx from 'clsx';

interface Props extends React.HTMLAttributes<HTMLDivElement> {}

const FootNote: React.FC<Props> = ({ className, children, ...rest }) => {
  return (
    <div className={clsx('footnote', className)} {...rest}>
      {children}
    </div>
  );
};

export default FootNote;
