import update from 'immutability-helper';
import { AnyAction } from 'redux';
import {
  FIND_WORKSPACE,
  FIND_WORKSPACE_SUCCESS,
  FIND_WORKSPACE_FAILED,
  SIGNIN_WORKSPACE,
  SIGNIN_WORKSPACE_SUCCESS,
  SIGNIN_WORKSPACE_FAILED
} from '../actions/auth';

export type State = {
  domain: {
    name: string;
    teamId: string;
    url: string;
    emailDomains: string[];
  };
  user: {
    id: string;
    email: string;
    token: string;
  };
  error: '';
  loading: boolean;
};

const initialState: State = {
  domain: {
    name: '',
    teamId: '',
    url: '',
    emailDomains: []
  },
  user: {
    id: '',
    email: '',
    token: ''
  },
  error: '',
  loading: false
};

const reducer = (state: State = initialState, action: AnyAction) => {
  switch (action.type) {
    case FIND_WORKSPACE:
      return update(state, {
        loading: { $set: true }
      });
    case FIND_WORKSPACE_SUCCESS:
      return update(state, {
        domain: { $set: action.payload },
        error: { $set: '' },
        loading: { $set: false }
      });
    case FIND_WORKSPACE_FAILED:
      return update(state, {
        error: { $set: action.payload },
        loading: { $set: false }
      });
    case SIGNIN_WORKSPACE:
      return update(state, {
        loading: { $set: true }
      });
    case SIGNIN_WORKSPACE_SUCCESS:
      return update(state, {
        error: { $set: '' },
        loading: { $set: false },
        user: { $set: action.payload }
      });
    case SIGNIN_WORKSPACE_FAILED:
      return update(state, {
        loading: { $set: false },
        error: { $set: action.payload }
      });
    default:
      return state;
  }
};

export default reducer;
