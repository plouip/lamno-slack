import auth, { State as AuthState } from './auth';

export interface RootState {
  auth: AuthState;
}

const reducers = {
  auth
};

export default reducers;
