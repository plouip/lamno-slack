import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Dispatch, bindActionCreators } from 'redux';
import { RootState } from '../../reducers';
import { signInWorkspace } from '../../actions/auth';

import AppHeader from '../../components/AppHeader';
import AppFooter from '../../components/AppFooter';
import PageBase from '../../components/PageBase';
import AccountLoginForm from '../../components/AccountLoginForm';
import Typography from '../../components/common/Typography';

export type Props = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> &
  RouteComponentProps;

interface State {
  email: string;
  password: string;
  rememberMe: boolean;
}

class Login extends React.Component<Props, State> {
  state = {
    email: '',
    password: '',
    rememberMe: false
  };

  onEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ email: e.target.value });
  };

  onPasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ password: e.target.value });
  };

  onRememberMeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ rememberMe: e.target.checked });
  };

  onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    Promise.resolve(
      this.props.signInWorkspace(
        this.state.email,
        this.state.password,
        this.state.rememberMe
      )
    )
      .then(() => {
        this.props.history.push('/workspace');
      })
      .catch(e => {});
  };

  render() {
    return (
      <React.Fragment>
        <AppHeader />
        <PageBase>
          <AccountLoginForm
            domain={this.props.domain.url}
            error={this.props.error}
            onEmailChange={this.onEmailChange}
            onPasswordChange={this.onPasswordChange}
            onRememberMeChange={this.onRememberMeChange}
            onSubmit={this.onSubmit}
            submitting={this.props.submitting}
            workspace={this.props.domain.name}
          />
          <Typography align="center">
            If you have an <strong>@{this.props.domain.name}.com</strong> email
            address, you can&nbsp;<a href="#">create an account</a>
          </Typography>
          <Typography align="center">
            Trying to create a workspace? &nbsp;
            <a href="#">Create a new workspace</a>
          </Typography>
        </PageBase>
        <AppFooter />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  domain: state.auth.domain,
  error: state.auth.error,
  submitting: state.auth.loading
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      signInWorkspace
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Login);
