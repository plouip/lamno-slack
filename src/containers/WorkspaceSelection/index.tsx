import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { RootState } from '../../reducers';
import { bindActionCreators, Dispatch } from 'redux';
import { findWorkspace } from '../../actions/auth';

import AppHeader from '../../components/AppHeader';
import AppFooter from '../../components/AppFooter';
import PageBase from '../../components/PageBase';
import WorkspaceSelectionForm from '../../components/WorkspaceSelectionForm';
import Typography from '../../components/common/Typography';

export type Props = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> &
  RouteComponentProps;

interface State {
  workspaceName: string;
}

class WorkspaceSelection extends React.Component<Props, State> {
  state = {
    workspaceName: ''
  };

  onSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    Promise.resolve(this.props.findWorkspace(this.state.workspaceName))
      .then(() => {
        this.props.history.push('/login');
      })
      .catch(e => {});
  };

  onWorkspaceNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ workspaceName: e.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <AppHeader />
        <PageBase>
          <WorkspaceSelectionForm
            error={this.props.error}
            onSubmit={this.onSubmitForm}
            onWorkspaceNameChange={this.onWorkspaceNameChange}
            submitting={this.props.loading}
          />
          <Typography align="center">
            Need to get your group started on Slack? &nbsp;
            <a href="#">Create a new workspace</a>
          </Typography>
        </PageBase>
        <AppFooter />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  loading: state.auth.loading,
  error: state.auth.error
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      findWorkspace
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(WorkspaceSelection);
