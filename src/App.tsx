import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import createStore from './store/createStore';
import { createBrowserHistory } from 'history';
import Routes from './routes';

const history = createBrowserHistory();
const store = createStore(history, {});
const App: React.FC = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Routes />
    </ConnectedRouter>
  </Provider>
);

export default App;
