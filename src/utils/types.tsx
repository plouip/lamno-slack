import { ThunkAction } from 'redux-thunk';
import { RootState } from '../reducers';
import { AnyAction } from 'redux';

export type ThunkResult<T> = ThunkAction<T, RootState, null, AnyAction>;
